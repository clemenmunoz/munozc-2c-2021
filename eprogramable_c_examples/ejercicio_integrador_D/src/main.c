/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"
/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;


#define CANT_BITS 4
uint8_t numero_BCD;


void funcion_7seg (uint8_t num_BCD, gpioConf_t vector[4]);
/*==================[internal functions declaration]=========================*/

void funcion_7seg (uint8_t num_BCD, gpioConf_t vector[4]){
	uint8_t i;
	uint8_t variable_auxiliar;
	for(i=0; i<CANT_BITS; i++){

		variable_auxiliar= num_BCD & (0x01);
		printf("Se prendio el led: %d", i+1);
		printf("\n\r");
		printf("->Puerto: %d", vector[i].port);
		printf("\n\r");
		printf("->Pin: %d", vector[i].pin);
		printf("\n\r");
		printf("->Direccion: %d", vector[i].dir);
		printf("\n\r");
		num_BCD=num_BCD>>1;
	}

}

int main(void)
{
	gpioConf_t vec[4] = {{1,4,1},{1,2,3},{1,6,4},{2,4,5}};
	numero_BCD=9;
	funcion_7seg(numero_BCD, vec);

	return 0;
}

/*==================[end of file]============================================*/

