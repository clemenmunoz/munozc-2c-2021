/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"
/*==================[macros and definitions]=================================*/

uint32_t V=0x01020304;
uint8_t a;
uint8_t b;
uint8_t c;
uint8_t d;

union corrimiento {

	struct corrimiento2{
		uint8_t a2;
		uint8_t b2;
		uint8_t c2;
		uint8_t d2;
	} variables2;
	uint32_t V2;
} nombre_union;


/*==================[internal functions declaration]=========================*/

int main(void)
{
   a=V;
   b=(V>>8);
   c=(V>>16);
   d=(V>>24);

   printf("V: %d",V);
   printf("\n\r");
   printf("a: %d",a);
   printf("\n\r");
   printf("b: %d",b);
   printf("\n\r");
   printf("c: %d",c);
   printf("\n\r");
   printf("d: %d",d);


   nombre_union.V2=0x01020304;
   nombre_union.variables2.a2=nombre_union.V2;
   nombre_union.variables2.b2=(nombre_union.V2>>8);
   nombre_union.variables2.c2=(nombre_union.V2>>16);
   nombre_union.variables2.d2=(nombre_union.V2>>24);

   printf("V2: %d",nombre_union.V2);
   printf("\n\r");
   printf("a2: %d",nombre_union.variables2.a2);
   printf("\n\r");
   printf("b2: %d",nombre_union.variables2.b2);
   printf("\n\r");
   printf("c2: %d",nombre_union.variables2.c2);
   printf("\n\r");
   printf("d2: %d",nombre_union.variables2.d2);



	return 0;
}

/*==================[end of file]============================================*/

