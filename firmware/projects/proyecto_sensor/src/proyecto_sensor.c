/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../../proyecto_sensor/inc/template.h"    /* <= own header */

#include "hc_sr4.h"
#include "systemclock.h"
#include "gpio.h"
#include "switch.h"
#include "led.h"
#include "DisplayITS_E0803.h"


/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	LedsInit();
	SwitchesInit();
	int16_t valor_sensor=0,a=1;
	uint8_t teclas;
	uint8_t encender;
	uint8_t hold=false;
	gpio_t pin[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
	ITSE0803Init(pin);
	SystemClockInit();

	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);

	while(a==1){

		if (encender == true) {
			valor_sensor= HcSr04ReadDistanceCentimeters();
		if(hold == false){
					if (valor_sensor > 0 && valor_sensor <= 10) { //Si la distancia está entre 0 y 10 cm, encender el LED_RGB_B (Azul).
						LedOn(LED_RGB_B);
						LedOff(LED_1);
						LedOff(LED_2);
						LedOff(LED_3);
					}
					if (valor_sensor > 10 && valor_sensor <= 20) { //Si la distancia está entre 0 y 10 cm, encender el LED_RGB_B (Azul) y LED1.
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOff(LED_2);
						LedOff(LED_3);
					}
					if (valor_sensor > 20 && valor_sensor <= 30) { //Si la distancia está entre 20 y 30 cm, encender el LED_RGB_B (Azul), LED1 y LED2.
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOn(LED_2);
						LedOff(LED_3);
					}
					if (valor_sensor > 30 && valor_sensor <= 130) { //Si la distancia es mayor a 30cm encende todos los leds
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOn(LED_2);
						LedOn(LED_3);
					}
					ITSE0803DisplayValue (valor_sensor);

		}
		if(hold==true){

		}
		}
		if(encender==false){
			LedOff(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			ITSE0803DisplayValue (0);

		}

		teclas = SwitchesRead();
		switch(teclas)
		    	{	case SWITCH_1:
			    		encender=!encender;
			    		DelayMs(100);
			    	break;
		    		case SWITCH_2:
		    		    hold=!hold;

		    		break;}


	DelayMs(200);


	}

	//HcSr04Deinit(GPIO_T_FIL2, GPIO_T_FIL3);

	return 0;
}

/*==================[end of file]============================================*/

