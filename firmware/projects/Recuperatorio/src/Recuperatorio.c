/*! @mainpage Proyecto Recuperatorio
 *
 * \section genDesc General Description
 *
 * Este sistema es capaz de transportar cajas vacias hacia el lugar de llenado.
 * Alli se frena la cinta 1.
 * Una vez en dicho lugar, se debe activar otra cinta transportadora que lleve
 * los objetos con los que se van a llenar la caja.
 * Luego que estas se llenen todas hasta un mismo peso (se utiliza una balanza para
 * comprobar peso) se vuelve a activar la cinta 1 para llenar otra caja.
 *
 * \section hardConn Hardware Connection
 *
 * |   Infrarrojo	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	 T_COL0	    |
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 *
 * |   Balanza		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	 T_COL0	    |
 * | 	+3.3V 	 	| 	  +3.3V    	|
 * | 	GND	    	| 	  GND		|
 *

 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 11/11/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Munoz Clementina
 *

/*==================[inclusions]=============================================*/
#include "balanza.h";
#include "led.h"
#include "Tcrt5000.h"
#include "uart.h"
#include "switch.h"
#include "led.h"
/*==================[macros and definitions]=================================*/
float peso=0;
bool encontro_caja=FALSE;
bool Cinta1=false;
bool Cinta2=false;
float peso_caja=0;
float pesoMaximo=20;
float peso_inicial=0;
uint8_t temporizador=0;
uint8_t contador_cajas=0;
uint8_t tiempo_llenado_MAX=0;
uint8_t tiempo_llenado_MIN=0;
uint8_t encender;
uint8_t detener=false;
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
/** @fn void doTimmer ()
 * @brief Sincroniza el funcionamiento de la Edu-Cia con interrupciones
 * @param[in] No parameter
 * @return void
 */
void doTimmer ();
/** @fn void float ConversorVoltajeKG(void)
 * @brief Convierte el valor del voltaje entregado por la balanza a KG
 * @param[in] No parameter
 * @return devuelve peso convertido
 */
float ConversorVoltajeKG(void)
/** @fn void doSwitch1(void)
 * @brief interpreta la tecla apretada por el usuario y enciende la cinta.
 * @param[void]
 * @return void
 */
void doSwitch1(void);
/** @fn void doSwitch2(void)
 * @brief interpreta la tecla apretada por el usuario y apaga la cinta.
 * @param[void]
 * @return void
 */
void doSwitch2(void);
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

float ConversorVoltajeKG(void){

	peso=0
	float voltaje_recibido=0;
	voltaje_recibido= CoversorAnalogVoltaje();
	peso=voltaje_recibido/0.022;
	return(peso);
}

void DoTimmer(void){

	if(encender==true){
	//Primero pregunto si la caja esta vacia
	peso_inicial=ConversorVoltajeKG();
	if(peso_inicial==0){
		cinta1=true;
	}
	else{
		cinta1=false;
	}
	if(Cinta1==true){// cinta1 va a ser true cuando peso de la caja sea 0.
		LedOn(LED_1);//prendo el led
		//Supongo que el sensor se encuentra en la balanza. Por ende si encuentra
		//un objeto, freno la cinta 1 y activo la cinta 2.
		encontro_caja = Tcrt5000State();
		if(encontro_caja == TRUE){
			LedOff(LED_1);
			Cinta2=true;
		}
	}
	if(Cinta2==true){
		contador_cajas++;
		LedOn(LED_2);//prendo el led
		peso_caja=ConversorVoltajeKG();
		while(peso_caja<pesoMaximo){//tiempo de llenado
			temporizador++;
		}
		if(peso_caja>pesoMaximo){

			if(temporizador<tiempo_llenado_MIN=0){
				tiempo_llenado_MIN=temporizador;
			}
			if(temporizador>tiempo_llenado_MAX=0){
				tiempo_llenado_MAX=temporizador;
			}
			LedOff(LED_2);
			Cinta1=true;
			Cinta2=false;

			UartSendString(SERIAL_PORT_PC, "Tiempo de llenado de caja ");
			UartSendString(SERIAL_PORT_PC, UartItoa(contador_cajas, 10));
			UartSendString(SERIAL_PORT_PC, UartItoa(temporizador, 10));
			UartSendString(SERIAL_PORT_PC, "seg \r\n");

			temporizador=0;
		}

		if(contador_cajas==15){
			UartSendString(SERIAL_PORT_PC, "Tiempo de llenado maximo ");
			UartSendString(SERIAL_PORT_PC, UartItoa(tiempo_llenado_maximo, 10));
			UartSendString(SERIAL_PORT_PC, "seg \r\n");

			UartSendString(SERIAL_PORT_PC, "Tiempo de llenado minimo ");
			UartSendString(SERIAL_PORT_PC, UartItoa(tiempo_llenado_minimo, 10));
			UartSendString(SERIAL_PORT_PC, "seg \r\n");

			contador_cajas=0;
			}
		}
	}
	if(detener=true){
		LedsOffAll();
	}
}

void doSwitch1(void){
	encender=true;
}
void doSwitch2(void){
	detener=true;
}

int main(void){

	LedsInit();
	SwitchesInit();
	SystemClockInit();

	timer_config my_timer = {TIMER_A,1000,&DoTimmer};
	TimerInit(&my_timer);
	TimerStart(TIMER_A);



	SwitchActivInt( 1 ,*doSwitch1 );
	SwitchActivInt( 2 ,*doSwitch2 );

	serial_config my_uart={SERIAL_PORT_PC, 115200, NULL};
	UartInit(&my_uart);

	while(1){

		/*Nada*/
	}

	return 0;

}




/*==================[end of file]============================================*/

