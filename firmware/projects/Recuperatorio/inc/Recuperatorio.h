/*! @mainpage Proyecto Recuperatorio
 *
 * \section genDesc General Description
 *
 * Este sistema es capaz de transportar cajas vacias hacia el lugar de llenado.
 * Alli se frena la cinta 1.
 * Una vez en dicho lugar, se debe activar otra cinta transportadora que lleve
 * los objetos con los que se van a llenar la caja.
 * Luego que estas se llenen todas hasta un mismo peso (se utiliza una balanza para
 * comprobar peso) se vuelve a activar la cinta 1 para llenar otra caja.
 *
 * \section hardConn Hardware Connection
 *
 * |   Ultrasound	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	 T_FIL2	    |
 * | 	TRIGGER	 	| 	 T_FIL3		|
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 * |  Potenciometer	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  VCC	    | 	   +5V	    |
 * | 	  OUT 	    | 	   CH1	    |
 * | 	  GND 	 	| 	   GND  	|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/11/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Munoz Clementina
 *
*/

#ifndef _RECUPERATORIO_H
#define _RECUPERATORIO_H

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _RECUPERATORIO_H */

