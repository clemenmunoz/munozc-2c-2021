/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../../proyecto_sensor/inc/template.h"    /* <= own header */

#include "hc_sr4.h"
#include "systemclock.h"
#include "gpio.h"
#include "switch.h"
#include "led.h"
#include "DisplayITS_E0803.h"
#include "timer.h"
#include "uart.h"


/*==================[macros and definitions]=================================*/
uint8_t encender;
uint8_t hold=false;
int16_t valor_sensor=0,a=1;
uint8_t teclas;
uint8_t dato_recibido;
uint8_t valor_convertido;

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
/** @fn void doSwitch1(void)
 * @brief interpreta la tecla apretada por el usuario y cambia de estado dicha tecla.
 * @param[void]
 * @return void
 */
void doSwitch1(void);
/** @fn void doSwitch2(void)
 * @brief interpreta la tecla apretada por el usuario y cambia de estado dicha tecla.
 * @param[void]
 * @return void
 */
void doSwitch2(void);
/** @fn void doTimer(void)
 * @brief da inicio al funcionamiento del programa, inidicandole mediante esta funcion
 * lo que debe realizar. Es decir, leer distancia del ultrasonido y encender leds dependiendo de
 * dicha lectura.
 * @param[void]
 * @return void
 */
void doTimer(void);
/** @fn void doUart(void)
 * @brief a partir de un dato recibido, lo interpreta y cambia el estado de las teclas.
 * @param[void]
 * @return void
 */
void doUart(void);
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

void doSwitch1(void){
	encender=!encender;
}
void doSwitch2(void){
	hold=!hold;
}
void doTimer(void){
	if (encender == true) {
				valor_sensor= HcSr04ReadDistanceCentimeters();
			if(hold == false){
						if (valor_sensor > 0 && valor_sensor <= 10) { //Si la distancia está entre 0 y 10 cm, encender el LED_RGB_B (Azul).
							LedOn(LED_RGB_B);
							LedOff(LED_1);
							LedOff(LED_2);
							LedOff(LED_3);
						}
						if (valor_sensor > 10 && valor_sensor <= 20) { //Si la distancia está entre 0 y 10 cm, encender el LED_RGB_B (Azul) y LED1.
							LedOn(LED_RGB_B);
							LedOn(LED_1);
							LedOff(LED_2);
							LedOff(LED_3);
						}
						if (valor_sensor > 20 && valor_sensor <= 30) { //Si la distancia está entre 20 y 30 cm, encender el LED_RGB_B (Azul), LED1 y LED2.
							LedOn(LED_RGB_B);
							LedOn(LED_1);
							LedOn(LED_2);
							LedOff(LED_3);
						}
						if (valor_sensor > 30 && valor_sensor <= 130) { //Si la distancia es mayor a 30cm encende todos los leds
							LedOn(LED_RGB_B);
							LedOn(LED_1);
							LedOn(LED_2);
							LedOn(LED_3);
						}
						ITSE0803DisplayValue (valor_sensor);
						//valor_convertido = UartItoa(valor_sensor, 10);
						UartSendString(SERIAL_PORT_PC, UartItoa(valor_sensor, 10));
						UartSendString(SERIAL_PORT_PC, " cm \r\n");

			}
			if(hold==true){

			}
			}
			if(encender==false){
				LedOff(LED_RGB_B);
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
				ITSE0803DisplayValue (0);

			}

}
void doUart(void){

	UartReadByte(SERIAL_PORT_PC, &dato_recibido);

	if(dato_recibido=='O'){
		encender=!encender;

	}
	if(dato_recibido=='H'){
		hold=!hold;
	}

}

int main(void){

	LedsInit();
	SwitchesInit();


	gpio_t pin[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
	ITSE0803Init(pin);
	SystemClockInit();

	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);

	SwitchActivInt( 1 ,*doSwitch1 );
	SwitchActivInt( 2 ,*doSwitch2 );

	timer_config my_timer = {TIMER_A,1000,&doTimer};

	serial_config my_uart={SERIAL_PORT_PC, 115200, doUart};

	TimerInit(&my_timer);
	TimerStart(TIMER_A);


	UartInit(&my_uart);

	while(a==1){


	}

	//HcSr04Deinit(GPIO_T_FIL2, GPIO_T_FIL3);

	return 0;
}

/*==================[end of file]============================================*/

