/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * La aplicacion es capaz de entregar la hora, medir la temperatura, medir la humedad y
 * por utlimo, medir la distancia reccorida por el usuario.
 * Todos estos datos son enviados por bluetooth a una aplicacion la cual ya esta disenada
 * para recibir los mismos.
 *
 * \section hardConn Hardware Connection
 *
 * | 	HC05		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC	 		| 	3,3V		|
 * |	GND			|	GND			|
 * |	TXD			|	232_RX		|
 * |	RXD			|	232_TX		|
 *
 * | 	DHT11		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	3,3V		|
 * |	DATA		|	GPIO_5		|
 * |	GND			|	GND			|
 *
 * | 	MMA7260Q	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Z		 	| 	CH3			|
 * | 	Y			|	CH2			|
 * |	X			|	CH1			|
 * |	gS1			|	GPIO_T_FLI2	|
 * |	gS2			|	GPIO_T_FIL1	|
 * |	VCC			|	3,3V		|
 * | 	GND			|	GND			|
 * |	SM			|	3.3V		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/11/2021 | MiSmartWatch	                         |
 * |			|							                     |
 *
 * @author Munoz Clementina
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/MiSmartWatch.h"       /* <= own header */
#include "acelerometro.h"
#include "uart.h"
#include "sapi_dht11.h"
#include "timer.h"
#include "sapi_rtc.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/
float valor_aceleracion = 0.0;
float valor_aceleracion_anterior = 1200.0;
float valor_distancia = 0.0;


int16_t contador = 0;
float valor_humedad = 0;
float valor_temperatura = 0;

bool condicion_Dht11;

rtc_t my_time = {2021, 11, 14, 1, 20, 10 ,20};

float valor_hora = 0;


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
/**@fn void doTimer()
 * @brief  Función que recibe el valor del acelerometro, y lo convierte a distancia recorrida.
 * Ademas, lee el reloj de la computadora y envia esos datos por bluetooth a la app.
 * @return void
 */
void doTimer(){
	valor_aceleracion = ReadXValue()*1000;
	UartSendString(SERIAL_PORT_PC,UartItoa((uint16_t)valor_aceleracion,10));
	UartSendString(SERIAL_PORT_PC,"\r\n");

	if((valor_aceleracion<800.0)&&(valor_aceleracion_anterior>800.0)){
		contador++;
		valor_distancia=(float)contador*0.80;
		UartSendString(SERIAL_PORT_P2_CONNECTOR,"*A");
		UartSendString(SERIAL_PORT_P2_CONNECTOR,UartItoa((uint16_t) valor_distancia, 10));
		UartSendString(SERIAL_PORT_P2_CONNECTOR,"*");
	}
	valor_aceleracion_anterior=valor_aceleracion;

	rtcRead(&my_time);

	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*D");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,UartItoa(my_time.hour,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR,":");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,UartItoa(my_time.min,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*");


}
/**@fn void doDht11()
 * @brief  Funcion que lee el valor de la humedad y de la temperatura; y los envia por bluetooth
 * a la app.
 * @return void
 */
void doDht11(){

	condicion_Dht11 = dht11Read(&valor_humedad,&valor_temperatura);

	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*H");
	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa ((uint16_t) valor_humedad,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*");

	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*T");
	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa ((uint16_t) valor_temperatura,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*");

}


int main(void){

	//Acelerometro
	MMA7260QInit(GPIO_T_FIL2, GPIO_T_FIL3);
    timer_config my_timer1 = {TIMER_A,100,&doTimer};
	TimerInit(&my_timer1);
	TimerStart(TIMER_A);

	//Sensor temperatura y humedad
	dht11Init(GPIO_5);
	timer_config my_timer2 = {TIMER_B,1000,&doDht11};
	TimerInit(&my_timer2);
	TimerStart(TIMER_B);

	//Modulo bluetooth para app
	serial_config my_uart={SERIAL_PORT_P2_CONNECTOR, 9600, NULL};
	UartInit(&my_uart);

	//Hora para el reloj
	rtcConfig(&my_time);

	//Senal aceleracion para ver por el serial plot
	serial_config my_uart2={SERIAL_PORT_PC, 115200, NULL};
	UartInit(&my_uart2);


	while (1){

	}

	return 0;
}

/*==================[end of file]============================================*/

