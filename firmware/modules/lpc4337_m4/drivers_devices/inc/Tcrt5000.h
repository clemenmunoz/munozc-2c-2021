	/* Copyright 2021,
 * Maria Casablanca
 * mariacasabla@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef TCRT5000_H
#define TCRT5000_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Infrarrojo
 ** @{ */

/** @brief Bare Metal header for sensor TCRT5000 on EDU-CIAA NXP
 **
 ** This is a driver for TCRT5000
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	MC         Maria Casablanca
 *	JT         Julian Traversaro
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210901 v0.1 initials initial version Julian Traversaro
 * 20210923 v1.1 new version made by Maria Casablanca
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "delay.h"
#include "gpio.h"

/*==================[macros]=================================================*/
#define lpc4337            1
#define mk60fx512vlq15     2

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/


/*==================[external functions declaration]=========================*/

/** @fn bool Tcrt5000Init(gpio_t dout)
 * @brief Inicializacion funcion del sensor TCRT5000 de la EDU-CIAA
 * @param[in] dout
 * @return TRUE if no error
 */
bool Tcrt5000Init(gpio_t dout);

/** @fn bool Tcrt5000State(void)
 * @brief Indica el estado del pin
 * @param[in] No parameter
 * @return Booleano que indica el estado del pin
 */
bool Tcrt5000State(void);

/** @fn bool Tcrt5000Deinit(gpio_t dout)
 * @brief Desinicializacion funcion del sensor TCRT5000 de la EDU-CIAA
 * @param[in] dout
 * @return TRUE if no error
 */
bool Tcrt5000Deinit(gpio_t dout);

/*==================[end of file]============================================*/





#endif /* #ifndef TCRT5000_H */

