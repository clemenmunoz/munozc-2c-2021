#ifndef GENIOMETRO_H
#define GENIOMETRO_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup GENIOMETRO geniometro
 ** @{ */

/** @brief Este es un controlador para adquirir el angulo que porporciona un potenciometro
 **
 **
 **
 **/

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211101v1.1 new version made by Joaquin Palandri
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "analog_io.h"

/*==================[macros]=================================================*/
#define lpc4337            1
#define mk60fx512vlq15     2

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/
/** @fn void geniometroInit();
 * @brief Initialization el pin analógico
 * @param[in]
 * @return
 */
void geniometroInit();


/** @fn uint16_t geniometroRead();
 * @brief Realiza la lectura del Sensor.
 * @param[in]
 * @return el valor que se lee
 */
uint16_t geniometroRead ();

/** @fn uint16_t geniometroConvertirAngulo ();
 * @brief Toma un valor analógio y lo relaciona a una escala de angulos
 * @param[in]
 * @return el valor del angulo correspondiente.
 */
uint16_t geniometroConvertirAngulo (uint16_t angulo);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */


/*==================[end of file]============================================*/
#endif /* #ifndef LED_H */

