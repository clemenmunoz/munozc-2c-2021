

/** \brief Bare Metal driver for leds in the EDU-CIAA board.
 **
 **/

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211101 v0.1 initials initial version Joaquin Palandri
 */

/*==================[inclusions]=============================================*/
//#include "geniometro.h"
#include "analog_io.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void geniometroInit(){
	analog_input_config puertoAnalogo={CH1, 0};
		AnalogInputInit(&puertoAnalogo);
}

uint16_t geniometroRead (){
uint16_t valorLeido;
	AnalogInputReadPolling(CH1,&valorLeido);
	return valorLeido;}


uint16_t geniometroConvertirAngulo (uint16_t angulo){
	uint16_t valorFinal=0;
	uint16_t aux=-15;
	for(uint8_t i=372; i<=684;i=i+52){
		aux=aux+15;
	if(angulo==i)
		valorFinal=aux;
		aux=-15;
	}
	return valorFinal;
}

/*==================[end of file]============================================*/
